import re
import email.charset


RE_QUOPRI_BS = re.compile(r'\b=20=\n')
RE_QUOPRI_LE = re.compile(r'\b=\n')
RE_LONG_WORDS = re.compile(r'\b[\w\/\+\=\n]{72,}\b')


email.charset.ALIASES.update({
    'iso-8859-8-i': 'iso-8859-8',
    'x-mac-cyrillic': 'mac-cyrillic',
    'macintosh': 'mac-roman',
    'windows-874': 'cp874',
    # manually fix unknown charset encoding
    'default': 'utf-8',
    'x-unknown': 'utf-8',
    '%charset': 'utf-8',
})


def extract_body(msg, body=None):
    """ Extract content body of an email messsage """
    if not body:
        body = []
    if msg.is_multipart():
        for part in msg.get_payload():
            if part.is_multipart():
                extract_body(part, body)
            else:
                if part.get_content_type().startswith("text/"):
                    body.append(part.get_payload())
    elif msg.get_content_type().startswith("text/"):
        charset = msg.get_param('charset', 'utf-8').lower()
        charset = email.charset.ALIASES.get(charset, charset)
        msg.set_param('charset', charset)
        try:
            body.append(msg.get_payload())
        except AssertionError as e:
            print('Parsing failed.    ')
            print(e)
        except LookupError:
            # set all unknown encoding to utf-8
            # then add a header to indicate this might be a spam
            msg.set_param('charset', 'utf-8')
            body.append('=== <UNKOWN ENCODING POSSIBLY SPAM> ===')
            body.append(msg.get_payload())
    return body


def all_bodies(msg):
    body = '\n\n'.join(extract_body(msg))
    # remove potential quote print formatting strings
    body = RE_QUOPRI_BS.sub('', body)
    body = RE_QUOPRI_LE.sub('', body)
    body = RE_LONG_WORDS.sub('', body)
    return body

import email.message


PRIMARY_KEY = 'Message-ID'


class Message(email.message.Message):
    @property
    def message_id(self):
        message_id = self.get(PRIMARY_KEY)
        if not message_id:
            raise KeyError("No Message-ID in the message")
        return message_id

    def get_body(self):
        self.body = ""
        if self.is_multipart():
            for part in self.walk():
                ctype = part.get_content_type()
                cdispo = str(part.get('Content-Disposition'))
                if ctype == 'text/plain' and 'attachment' not in cdispo:
                    self.body = part.get_payload(decode=True)
                    break
        else:
            ctype = self.get_content_type()
            self.body = self.get_payload(decode=True)
        return ctype, self.body

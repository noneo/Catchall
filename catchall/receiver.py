#!/usr/bin/env python3


# Author: Miroslav Houdek miroslav.houdek at gmail dot com
# License is, do whatever you wanna do with it (at least
# I think that that is what LGPL v3 says)


import smtpd
import email
import asyncore
import storage
import message


class CustomSMTPServer(smtpd.SMTPServer):
    def process_message(self, peer, mailfrom, rcpttos, data, **kwargs):
        msg = email.message_from_bytes(data)
        msg.__class__ = message.Message
        try:
            mail_storage.store(msg)
        except Exception as e:
            print(str(e))


mail_storage = storage.MailDB()
server = CustomSMTPServer(('127.0.0.1', 10025), None)
asyncore.loop()

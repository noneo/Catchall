import psycopg2 as psql
import psycopg2.extras as extras


class MailDB(object):

    def __init__(self):
        self._connection = None
        self.password = self.get_password()
        self.duplicating_headers = ['Received', 'DKIM-Signature']

    def get_password(self):
        _password_file = '../password'
        with open(_password_file) as fh:
            return dict(
                    [[field.strip() for field in line.split(':')]
                        for line in fh.readlines()])

    def __del__(self):
        if self._connection:
            self._connection.close()

    @property
    def connection(self):
        if self._connection:
            return self._connection
        try:
            self._connection = psql.connect(
                dbname=self.password['db_name'],
                user=self.password['db_user'],
                password=self.password['db_pass'])
        except psql.OperationalError:
            print("Couldn't connect to the database")
            raise
        return self._connection

    def __commit_or_rollback(func):
        # TODO Set autocommit and wipe out the decorator
        """
        Inserts new cursor to wrapped function.
        No need to pass it or create separately
        Function must be invoked as

        @__commit_or_rollback
        def funcname(self, cursor, anything_else):

        and called

        funcname(anything_else)
        """

        def wrapper(*args):
            self = args[0]
            cursor = self.connection.cursor()
            args = (args[0], cursor) + args[1:]
            try:
                func(*args)
            except Exception:
                self.connection.rollback()
                raise
            self.connection.commit()
        return wrapper

    @__commit_or_rollback
    def insert_header(self, cursor, message_id, header, value):
        """
        TODO:
        https://stackoverflow.com/questions/8134602/psycopg2-insert-multiple-rows-with-one-query

        """
        if header == 'Received':
            self.insert_received(message_id, header, value)
            return
        try:
            cursor.execute("INSERT INTO mails VALUES (%s, %s, %s)",
                           (message_id, header, value))
        except psql.IntegrityError as e:
            print(str(e))

    @__commit_or_rollback
    def insert_headers(self, cursor, message_id, headers):
        filtered_headers = [
                (message_id, header, value) for header, value in headers
                if header not in self.duplicating_headers]
        insert_statement = 'INSERT INTO mails VALUES %s'
        extras.execute_values(cursor, insert_statement, filtered_headers,
                              template=None, page_size=100)
        filtered_headers = [
                (message_id, header, value) for header, value in headers
                if header in self.duplicating_headers]
        insert_statement = 'INSERT INTO received VALUES %s'
        extras.execute_values(cursor, insert_statement, filtered_headers,
                              template=None, page_size=100)

    @__commit_or_rollback
    def insert_received(self, cursor, message_id, header, value):
        try:
            cursor.execute("INSERT INTO received VALUES (%s, %s, %s)",
                           (message_id, header, value))
        except psql.IntegrityError as e:
            print(str(e))

    @__commit_or_rollback
    def insert_body(self, cursor, message_id, content_type, body):
        try:
            cursor.execute(
                    """
                    INSERT INTO bodies (message_id, content_type, body)
                    VALUES (%s, %s, %s)
                    """, (message_id, content_type, body))
        except psql.IntegrityError as e:
            print(str(e))

    def store(self, msg):
        self.insert_headers(msg.message_id, msg.items())
        content_type, body = msg.get_body()
        self.insert_body(msg.message_id, content_type, body)

#!/usr/bin/env python3

import getpass
import unittest

from catchall import processor

class TestCatchall(unittest.TestCase):
    def setUp(self):
        self.loglevel = processor.DEFAULT_LOGLEVEL
        self.who_am_i = getpass.getuser()
        self.socket_name = processor.DEFAULT_SOCKET_PATH

    def test_socket_name(self):
        socket_name = '/home/{}/local/var/sockets/catchall.sock'.format(self.who_am_i)
        self.assertEqual(self.socket_name, socket_name, "Wrong socket path")

    def test_log_level(self):
        loglevel = 'WARNING'
        self.assertEqual(self.loglevel, loglevel, "Wrong log level")

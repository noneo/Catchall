import unittest
import email
from catchall import storage, message


PRIMARY_KEY = storage.PRIMARY_KEY  # Usually Message-ID


class TestStorage(unittest.TestCase):

    def setUp(self):
        self.mail_file = './test/pgsql-annouce.mail'
        self.message = self.load_message_from_filename()
        self.message_id = self.message.message_id
        self.maildb = storage.MailDB()

    def load_message_from_filename(self):
        with open(self.mail_file) as fh:
            return message.Message(email.message_from_file(fh))

    def test_storing_headers(self):
        self.maildb.save_headers(self.message)

    def test_storing_body(self):
        self.maildb.save_bodies(self.message)

    def tearDown(self):
        self.maildb.remove_id(self.message_id)
